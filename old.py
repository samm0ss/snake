__author__ = 'SAM'
class Apples(object):
    def __init__(self):
        self.apples = []
        self.color = (200, 50, 100)
        self.size = 10

    def create(self,number):
        for i in range(0,number):
            self.apples.append([random.randint(0,200), random.randint(0,200)])

    def draw(self,screen):
        for i,coordinate in enumerate(self.apples):
            pygame.draw.circle(screen, self.color, coordinate, self.size)
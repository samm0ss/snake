__author__ = 'SAM'

import pygame
import math
import random

pygame.quit()
screenSize = (400, 400)
screen = pygame.display.set_mode(screenSize)
pygame.display.set_caption("Mario what!")
clock = pygame.time.Clock()

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

class Snake(object):
    def __init__(self):
        self.x = random.randint(0,200)
        self.y = random.randint(0,200)
        self.snake = [[self.x,self.y]]
        self.change_x = 0
        self.change_y = 0
        self.color = (0, 0, 0)
        self.size = 10

    def prepend(self):
        self.snake.insert(0,[self.snake[0][0] + (self.change_x*self.size*2), self.snake[0][1] + (self.change_y*self.size*2)])

    def move(self):
        self.prepend()
        self.snake.pop(-1)

    def draw(self,screen):
        for i,coordinate in enumerate(self.snake):
            pygame.draw.circle(screen, self.color, coordinate, self.size)


class Apple(object):
    def __init__(self,size):
        self.color = (200, 50, 100)
        self.size = size
        self.x = 0
        self.y = 0

    def relocate(self):
        self.x = random.randint(0,200)
        self.y = random.randint(0,200)

    def draw(self,screen):
        pygame.draw.circle(screen, self.color, [self.x,self.y], self.size)

    def surface(self):
        pass


apple_list = []
for i in range(30):
    apple = Apple(10)
    apple.relocate()
    apple_list.append(apple)

running = True
snake = Snake()


while running:
    clock.tick(30)

    # EVENT HANDLING
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT and snake.change_x!=1:
                snake.change_x = -1
                snake.change_y = 0
            elif event.key == pygame.K_RIGHT and snake.change_x!=-1:
                snake.change_x = 1
                snake.change_y = 0
            elif event.key == pygame.K_UP and snake.change_y!=1:
                snake.change_x = 0
                snake.change_y = -1
            elif event.key == pygame.K_DOWN and snake.change_y!=-1:
                snake.change_x = 0
                snake.change_y = 1
            elif event.key == pygame.K_0:
                snake.prepend()


    # RUN CALCULATIONS/GAME LOGIC

    # DRAWING
    screen.fill(RED)  # Clear screen
    #do drawing
    snake.move()
    snake.draw(screen)
    for apple in apple_list:
        apple.draw(screen)
    pygame.display.flip()  # update screen

    clock.tick(15)

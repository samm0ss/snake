import pygame
import math
import random

pygame.quit()
screenSize = (400, 400)
screen = pygame.display.set_mode(screenSize)
pygame.display.set_caption("Mario what!")
clock = pygame.time.Clock()

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

class Snake(object):
    def __init__(self):
        self.x = random.randint(0,200)
        self.y = random.randint(0,200)
        self.snake = [[self.x,self.y]]

        self.change_x = 1
        self.change_y = 0

        self.color = (0, 0, 0)
        self.size = 10
    def grow(self):
        self.snake += [[self.snake[0][0] + self.size, self.snake[0][1] + self.size]]
        print self.snake

    def move(self):
        for i,coordinate in enumerate(self.snake):
            self.snake[i][0] += self.change_x
            self.snake[i][1] += self.change_y

    def draw(self,screen):
        #pygame.draw.rect(screen, self.color, [self.x, self.y, self.size, self.size], 0)  # (x, y, width, height)
        for i,coordinate in enumerate(self.snake):
            pygame.draw.circle(screen, self.color, coordinate, self.size)

running = True
snake = Snake()

while running:
    clock.tick(15)

    # EVENT HANDLING
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            pass

    # RUN CALCULATIONS/GAME LOGIC

    # DRAWING
    screen.fill(RED)  # Clear screen
    #do drawing
    snake.draw(screen)
    snake.move()
    #pygame.draw.rect(screen, BLACK, [30, 30, 10, 10], 0)  # (x, y, width, height)
    # pygame.draw.ellipse(screen, BLACK, [20, 20, 250, 100], 2)
    # pygame.draw.arc(screen, GREEN, [100, 100, 250, 200],  0, math.pi, 2)
    # pygame.draw.polygon(screen, BLACK, [[100,100], [0,200], [200,200]], 5)
    #
    # pygame.draw.line(screen, GREEN, [0, 200], [100, 200], 5)
    pygame.display.flip()  # update screen

    clock.tick(15)



